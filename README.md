# Data Metrics #
This project is intended to be a one size fit all web application that aggregates interesting data from social media sites and drawing insights from them.

### What is currently available  ###

#### Twitter Metrics ####
* Get to know what is currently trending in kenya
* Get to know how many were original tweets vs retweets
* Get to know influencers of a trend
* Get to know what are the most used gadgets to tweet

### Roadmap ###
* Add sentiment analysis
* Add other social media site (Facebook and Instagram)
* Export insights to different document formats (PDF for graphical visualizations & excel for tabular data) 

### Projects Url ###
[http://datametrics.co.ke/twitter](Link URL)


### Have any suggestions or criticisms ? ###

contact Phil on twitter [https://twitter.com/philemonsamoei](@philemonsamoei)