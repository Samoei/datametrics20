import os
from app import create_app
from flask.ext.script import Manager, Server
from flask.ext.migrate import Migrate, MigrateCommand
from app import db
from app.models import User

app = create_app(os.getenv('FLASK_CONFIG') or 'default')

manager = Manager(app)
migrate = Migrate(app, db)

server = Server(host="0.0.0.0", port=5000)

manager.add_command("runserver", server)
manager.add_command('db', MigrateCommand)

@manager.command
def add_user(email, username, admin=False):
  """ Register a new user"""

  from getpass import getpass
  password = getpass()
  password2 = getpass(prompt='Enter Password Again to Confirm: ')
  if password != password2:
    import sys
    sys.exit('Error: The passwords provided do not match.')
  db.create_all()
  user = User(email=email, username=username, password=password, is_admin=admin)
  db.session.add(user)
  db.session.commit()
  print('User {} was added successfully.'.format(username))


if __name__ == '__main__':
  manager.run()