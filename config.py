import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
	SECRET_KEY = os.environ.get('SECRET_KEY')
	UPLOAD_FOLDER = os.path.join(basedir, 'app/static/uploads')
	CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672//'
	CELERY_ACCEPT_CONTENT = ['pickle']
	CELERY_TASK_SERIALIZER = 'pickle'
	CACHE_MEMCACHED_SERVERS = (['127.0.0.1:11211'])


class DevelopmentConfig(Config):
	DEBUG = True
	SECRET_KEY = os.environ.get('SECRET_KEY') or 't0p s3Cr3tZ'
	SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

	MAIL_SERVER = 'mail.datametrics.co.ke'
	MAIL_PORT = 26
	MAIL_USE_TLS = False
	MAIL_USE_SSL = False
	MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
	MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
	MAIL_SENDER = 'Data Metrics Admin <bot@datametrics.co.ke>'
	MAIL_DEFAULT_SENDER = 'Data Metrics<bot@datametrics.co.ke>'
	MAIL_SUBJECT_PREFIX = '[Data Metrics]'


class TestingConfig(Config):
	TESTING = True
	SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')


class ProductionConfig(Config):
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
	MAIL_SERVER = 'mail.datametrics.co.ke'
	MAIL_PORT = 25
	MAIL_USE_TLS = False
	MAIL_USE_SSL = False
	MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
	MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
	MAIL_SENDER = 'Data Metrics Admin <bot@datametrics.co.ke>'
	MAIL_DEFAULT_SENDER = 'Data Metrics<bot@datametrics.co.ke>'
	MAIL_SUBJECT_PREFIX = '[Data Metrics]'


config = {
	'development': DevelopmentConfig,
	'testing': TestingConfig,
	'production': ProductionConfig,
	'default': DevelopmentConfig
}
