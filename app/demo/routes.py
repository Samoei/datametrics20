from flask import render_template
from . import demo


@demo.route('/skills')
def skills():
	return render_template('demo/skills.html')


@demo.route('/projects')
def projects():
	return render_template('demo/projects.html')