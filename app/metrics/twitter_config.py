import tweepy
import os, sys
from tweepy import OAuthHandler
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

def get_twitter_auth():
	try:
		consumer_key = os.environ["TWITTER_CONSUMER_KEY"]
		consumer_secret = os.environ["TWITTER_CONSUMER_SECRET"]
		access_token = os.environ["TWITTER_ACCESS_TOKEN"]
		access_secret = os.environ["TWITTER_ACCESS_SECRET"]
	except KeyError:
		sys.stderr.write("TWITTER_* environment variables not set\n")
		sys.exit(1)

	auth = OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_secret)

	return auth

def get_twitter_api():
	auth = get_twitter_auth()
	api  = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
	return api

def get_db_session():
	try:
		try:
			engine = create_engine(os.environ.get('DATABASE_URL'), echo=False)
		except AttributeError, e:
			engine = create_engine(os.environ.get('DEV_DATABASE_URL'), echo=False)

	except OperationalError, e:
		sys.stderr.write("PROBLEM IN CONNECTING TO THE DATABASE\n")
		sys.exit(1)

	Base = declarative_base()
	Session = sessionmaker(bind = engine, autoflush=False)
	session = Session()
	return session
