from flask import render_template
from . import metrics
from ..models import TwitterTrend, TwitterUser, TrendTweet
from .. import db
from flask.ext.login import login_required
from sqlalchemy import func

from bokeh.plotting import figure
from bokeh.resources import CDN, INLINE
from bokeh.embed import file_html, components
from bokeh.charts import Bar

import pandas as pd
from bokeh.util.string import encode_utf8
# from bokeh.models import NumericalTickFormatter, ColumnDataSource



@metrics.route('/twitter')
@login_required
def twitter_home():
	top_users = db.session.query(TwitterUser).order_by(TwitterUser.followers_count.desc())[:10]
	top_trends = db.session.query(TwitterTrend).filter(TwitterTrend.trending == True).order_by(TwitterTrend.position)[:10]
	return render_template('metrics/index.html', top_trends=top_trends, top_users=top_users)


@metrics.route('/twitter/trends')
@login_required
def twitter_trends():
	top_trends = db.session.query(TwitterTrend).filter(TwitterTrend.trending == True).order_by(TwitterTrend.position)
	return render_template('metrics/twitter-trend.html', top_trends=top_trends)


@metrics.route('/twitter/trend/<int:id>/<order>')
@login_required
def twitter_trend(id, order='latest'):
	top_trend = db.session.query(TwitterTrend).filter(TwitterTrend.id == id).one()
	tweets = top_trend.tweets
	total_users = db.session.query(TrendTweet.tweeted_by).distinct().filter(TrendTweet.trend_id == id).count()
	# devices = tweets.query(func.count(TrendTweet.tweeted_using), TrendTweet.tweeted_using).group_by(TrendTweet.tweeted_using).order_by(func.count(TrendTweet.tweeted_using).desc()).all()
	devices = db.session.query(func.count(TrendTweet.tweeted_using), TrendTweet.tweeted_using).group_by(TrendTweet.tweeted_using).filter(TrendTweet.trend_id == top_trend.id).order_by(func.count(TrendTweet.tweeted_using).desc())[:5]

	df = pd.DataFrame(devices, columns=['Count', 'Source'])
	p = Bar(df, label='Source', values='Count', title="Top 5 Sources", color='#1ABB9C', responsive=True)
	p.logo = None
	p.toolbar_location = None

	css_resources = INLINE.render_css()
	js_resources = INLINE.render_js()
	script, div = components(p, INLINE)

	total_tweets = tweets.count()

	top_tweets = None
	if order == 'retweets':
		top_tweets = tweets.order_by(TrendTweet.retweet_count.desc())[:10]
	elif order == 'likes':
		top_tweets = tweets.order_by(TrendTweet.favourite_count.desc())[:10]
	elif order == 'latest':
		top_tweets = tweets.order_by(TrendTweet.tweeted_at.desc())[:10]
	html =  render_template('metrics/trend.html', top_trend=top_trend, total_tweets=total_tweets, devices=devices, total_users=total_users, top_tweets=top_tweets, order=order, script=script, div=div, js_resources=js_resources, css_resources=css_resources)

	return encode_utf8(html)


@metrics.route('/twitter/users')
@login_required
def twitter_users():
	twitter_users = db.session.query(TwitterUser).order_by(TwitterUser.followers_count.desc())
	return render_template('metrics/twitter-users.html', twitter_users=twitter_users)
