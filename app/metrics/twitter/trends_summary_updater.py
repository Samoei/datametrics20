from ..twitter_config import get_db_session
from ...models import TwitterTrend, TrendUserActivity, ScheduledTweets
from datetime import datetime, timedelta
import time
from status_updater import update_status

# Instantiate DB Session
session = get_db_session()

# WHAT TO TWEET
# 1. Top 3 actively tweeting for #SalvadoInKisumu were:@BettKMax (79 Tweets & RTs) @ElvoKibet (72) @Machukah (65)

# 2. Top 3 actively retweeting for #SalvadoInKisumu were:@BettKMax (79 Tweets & RTs) @ElvoKibet (72) @Machukah (65)

# 3. #FridayBriefing in numbers trended for 23 minutes with tweets(158) retweets(256) users(157)

# 4. On Friday 9, #MyNetwork was Trending Topic in Kenya for 16 hours:

# 5. The largest number of Trends in Kenya for Friday 9 was at 21 hours:



# Get all trends with no summary
def get_trends():
	half_day_ago = datetime.today() - timedelta(days=2)
	trends = session.query(TwitterTrend).filter(TwitterTrend.started > half_day_ago, TwitterTrend.ended != None,  TwitterTrend.summarized == None).all()
	return trends

def process_trends(trends):
	print "LENGTH OF TRENDS {}".format(len(trends))
	for trend in trends:
		tweet_active_tweeters(trend)
		time.sleep(60)
		tweet_active_retweeters(trend)
		time.sleep(60)
		# tweet_trend_numbers(trend)
		trend.summarized = True
		session.add(trend)
		session.commit()


def tweet_active_retweeters(trend):
	active_retweeters = session.query(TrendUserActivity).filter(TrendUserActivity.trend_id == trend.id, TrendUserActivity.retweets != None).order_by(TrendUserActivity.retweets.desc())[:3]
	print "FETCHED {} ACTIVE RETWEETERS\n".format(len(active_retweeters))
	trend_name = trend.name
	trend_id = trend.id
	status_list = []
	for active_user in active_retweeters:
		handle = active_user.screen_name
		retweets = active_user.retweets
		user_tuple = (handle, retweets)
		status_list.append(user_tuple)

	# print status_list
	if len(status_list) > 0 :
		try:
			status_str = "Top 3 actively retweeting for {} were: @{} ({}) @{} ({}) @{} ({})\nmore #KOTDATA http://datametrics.co.ke/twitter/trend/{}/".format(trend_name, status_list[0][0], status_list[0][1], status_list[1][0], status_list[1][1], status_list[2][0], status_list[2][1],trend_id)
		except IndexError:
			try:
				status_str = "Top 2 actively retweeting for {} were: @{} ({}) @{} ({})\nmore #KOTDATA http://datametrics.co.ke/twitter/trend/{}/".format(trend_name, status_list[0][0], status_list[0][1], status_list[1][0], status_list[1][1],trend_id)
			except IndexError:
				status_str = "Top 1 actively retweeting for {} were: @{} ({}) \nmore #KOTDATA http://datametrics.co.ke/twitter/trend/{}/".format(trend_name, status_list[0][0], status_list[0][1],trend_id)
		schedule_tweet(status_str, trend)
		print status_str


def tweet_active_tweeters(trend):
	active_users = session.query(TrendUserActivity).filter(TrendUserActivity.trend_id == trend.id, TrendUserActivity.tweets != None).order_by(TrendUserActivity.tweets.desc())[:3]
	print "FETCHED {} ACTIVE TWEETERS\n".format(len(active_users))
	trend_name = trend.name
	trend_id = trend.id
	status_list = []
	for active_user in active_users:
		handle = active_user.screen_name
		tweets = active_user.tweets
		user_tuple = (handle, tweets)
		status_list.append(user_tuple)

	# print status_list
	if len(status_list) > 0 :
		try:
			status_str = "Top 3 actively tweeting for {} were: @{} ({}) @{} ({}) @{} ({})\nmore #KOTDATA http://datametrics.co.ke/twitter/trend/{}/".format(trend_name, status_list[0][0], status_list[0][1], status_list[1][0], status_list[1][1], status_list[2][0], status_list[2][1],trend_id)
		except IndexError:
			try:
				status_str = "Top 2 actively tweeting for {} were: @{} ({}) @{} ({})\nmore #KOTDATA http://datametrics.co.ke/twitter/trend/{}/".format(trend_name, status_list[0][0], status_list[0][1], status_list[1][0], status_list[1][1],trend_id)
			except IndexError:
				status_str = "Top 1 actively tweeting for {} were: @{} ({})\nmore #KOTDATA http://datametrics.co.ke/twitter/trend/{}/".format(trend_name, status_list[0][0], status_list[0][1],trend_id)
		schedule_tweet(status_str, trend)
		print status_str


def schedule_tweet(status_str, trend):
	tweet = ScheduledTweets()
	tweet.trend_id = trend.id
	tweet.tweet_text = status_str
	session.add(tweet)
	try:
		session.commit()
	except:
		session.rollback()


if __name__ == '__main__':
	trends = get_trends()
	process_trends(trends)
