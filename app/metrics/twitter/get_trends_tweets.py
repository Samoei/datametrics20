from ..twitter_config import get_twitter_api, get_db_session
from ...models import TwitterTrend, TrendTweetFetchLog, TrendTweet, TrendTweetEntities
from datetime import datetime
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
import tweepy
import sys

api = get_twitter_api()
session = get_db_session()


def fetch_trends_tweets(trend, count=100, since_id=None):
	# Create Fetch Log
	fetch_log_id = create_fetch_log()
	# get tweets
	if since_id:
		for status in tweepy.Cursor(api.search, q=trend.query, since_id=since_id).items(count):
			# Check if the tweet is a retweet
			print ">>>>>>>>>>>>>>>>>>>>>>>>>> TRYING TO FECTH: {}".format((status.text).encode('utf-8').strip())
			try:
				if status.retweeted_status:
					# This is a retweet
					# get the original tweet
					save_retweet(status, trend, fetch_log_id)
					print ">>>>>>>>>>>>>>>>>>>>>>>>>> THIS IS A RETWEET:"
					# check if original tweet is already in the database

					try:
						tweet = session.query(TrendTweet).filter(TrendTweet.tweet_id == status.retweeted_status.id_str).one()
						# UPDATE USER DETAILS
						print ">>>>>>>>>>>>>>>>>>>>>>>>>> WE FOUND AND ID FOR THIS TWEET IN THE DB:".format(tweet.tweet_id)
						session.delete(tweet)
						session.commit()
						save_tweet(status.retweeted_status, trend, fetch_log_id)
					except NoResultFound:
						save_tweet(status.retweeted_status, trend, fetch_log_id)
					finally:
						session.commit()
			except:
				save_tweet(status, trend, fetch_log_id)

	else:
		for status in tweepy.Cursor(api.search, q=trend.query).items(count):
			try:
				if status.retweeted_status:
					# This is a retweet
					# get the original tweet
					save_retweet(status, trend, fetch_log_id)
					print ">>>>>>>>>>>>>>>>>>>>>>>>>> THIS IS A RETWEET:"
					# check if original tweet is already in the database

					try:
						tweet = session.query(TrendTweet).filter(TrendTweet.tweet_id == status.retweeted_status.id_str).one()
						# UPDATE USER DETAILS
						print ">>>>>>>>>>>>>>>>>>>>>>>>>> WE FOUND AND ID FOR THIS TWEET IN THE DB:".format(tweet.tweet_id)
						session.delete(tweet)
						session.commit()
						save_tweet(status.retweeted_status, trend, fetch_log_id)
					except NoResultFound:
						save_tweet(status.retweeted_status, trend, fetch_log_id)
					finally:
						session.commit()
			except:
				save_tweet(status, trend, fetch_log_id)
	# update fetch log
	update_fetch_log(fetch_log_id)


def save_retweet(status, trend, fetch_log_id):
	tweet = TrendTweet()
	tweet.trend_id = trend.id
	tweet.fetch_id = fetch_log_id
	tweet.retweet = True
	tweet.original_tweet_id = status.retweeted_status.id_str
	tweet.text = (status.text).encode('utf-8').strip()
	tweet.tweet_id = status.id_str
	tweet.tweeted_at = status.created_at
	if status.retweet_count:
		tweet.retweet_count = status.retweet_count
	if status.favorite_count:
		tweet.favourite_count = status.favorite_count
	if status.user.screen_name:
		tweet.tweeted_by = status.user.screen_name
	if status.user.profile_image_url:
		tweet.profile_img_url = (status.user.profile_image_url).replace("_normal", "")
	if status.user.name:
		tweet.user_name = (status.user.name).encode('utf-8').strip()
	if status.source:
		tweet.tweeted_using = (status.source).encode('utf-8').strip()
	if status.user.time_zone:
		tweet.tweeted_from = status.user.time_zone
	if status.source_url:
		tweet.source_url = status.source_url
	if status.user.friends_count:
		tweet.friends_count = status.user.friends_count
	if status.user.followers_count:
		tweet.followers_count = status.user.followers_count
	if status.user.statuses_count:
		tweet.statuses_count = status.user.statuses_count
	if status.user.verified:
		tweet.verified = status.user.verified
	try:
		session.add(tweet)
		session.commit()
	except IntegrityError:
		session.rollback()
		db_tweet = session.query(TrendTweet).filter(TrendTweet.tweet_id == status.id_str).one()
		# UPDATE USER DETAILS
		session.delete(db_tweet)
		session.commit()
		session.add(tweet)
		session.commit()


def save_tweet(status, trend, fetch_log_id):
	tweet = TrendTweet()
	tweet.trend_id = trend.id
	tweet.fetch_id = fetch_log_id
	tweet.text = (status.text).encode('utf-8').strip()
	tweet.tweet_id = status.id_str
	tweet.tweeted_at = status.created_at
	if status.retweet_count:
		tweet.retweet_count = status.retweet_count
	if status.favorite_count:
		tweet.favourite_count = status.favorite_count
	if status.user.screen_name:
		tweet.tweeted_by = status.user.screen_name
	if status.user.profile_image_url:
		tweet.profile_img_url = (status.user.profile_image_url).replace("_normal", "")
	if status.user.name:
		tweet.user_name = (status.user.name).encode('utf-8').strip()
	if status.source:
		tweet.tweeted_using = (status.source).encode('utf-8').strip()
	if status.user.time_zone:
		tweet.tweeted_from = status.user.time_zone
	if status.source_url:
		tweet.source_url = status.source_url
	if status.user.friends_count:
		tweet.friends_count = status.user.friends_count
	if status.user.followers_count:
		tweet.followers_count = status.user.followers_count
	if status.user.statuses_count:
		tweet.statuses_count = status.user.statuses_count
	if status.user.verified:
		tweet.verified = status.user.verified
	try:
		session.add(tweet)
		session.commit()
	except IntegrityError:
		session.rollback()
		db_tweet = session.query(TrendTweet).filter(TrendTweet.tweet_id == status.id_str).one()
		# UPDATE USER DETAILS
		session.delete(db_tweet)
		session.commit()
		session.add(tweet)
		session.commit()

	if status.retweet_count:
		print "Retweet Count: {}".format(status.retweet_count)
	if status.retweet_count:
		print "Retweet Count: {}".format(status.retweet_count)
	tweet.tweet_id = status.id
	tweet.tweet_id = status.id
	if status.entities.get('hashtags'):
		for item in status.entities.get('hashtags'):
			print "Hashtag: {}".format(item.get('text').encode('utf-8').strip())
	if status.entities.get('urls'):
		for item in status.entities.get('urls'):
			print "Url: {}".format(item.get('expanded_url'))
	if status.entities.get('user_mentions'):
		for item in status.entities.get('user_mentions'):
			print "Mentions: {}".format(item.get('screen_name'))
	print "Created: {}".format(status.created_at)
	if status.favorite_count:
		print "Favourite Count: {}".format(status.favorite_count)
	if status.in_reply_to_screen_name:
		print "Reply to screen name: {}".format(status.in_reply_to_screen_name)
	if status.retweet_count:
		print "Retweet Count: {}".format(status.retweet_count)
	if status.retweeted:
		print "Retweeted: {}".format(status.retweeted)
	if status.source:
		print "Source: {}".format((status.source).encode('utf-8').strip())
	if status.text:
		print "Tweet: {}".format((status.text).encode('utf-8').strip())
	if status.author:
		print "Author: {}".format(status.author.screen_name)
	if status.author:
		print "User Timezone: {}".format(status.author.time_zone)
	print "=========================================================\n"


def update_fetch_log(fetch_log_id):
	# try:
	# 	fetch_log = session.query(TrendTweetFetchLog).filter(TrendTweetFetchLog.id == fetch_log_id).one()
	# 	tweets_query = session.query(TrendTweet).filter(TrendTweet.fetch_id == fetch_log_id)
	# 	tweets_count = tweets_query.count()
	# 	last_tweet = tweets_query.order_by(TrendTweet.tweeted_at.desc())[:1]
	# 	fetch_log.total_tweets_fetched = tweets_count
	# 	fetch_log.last_tweet_id = last_tweet.tweet_id
	# 	session.add(fetch_log)
	# except NoResultFound:
	# 	sys.stderr.write("THE FETCH LOG WITH ID {} DOES NOT EXIST".format(fetch_log_id))
	# 	sys.exit(1)
	pass


def create_fetch_log():
	fetch_log = TrendTweetFetchLog()
	fetch_log.fetched_start = datetime.utcnow()
	session.add(fetch_log)
	session.commit()
	return fetch_log.id


if __name__ == '__main__':
	top_trends = session.query(TwitterTrend).filter(TwitterTrend.trending == True, TwitterTrend.position < 11).order_by(TwitterTrend.position)
	# top_trends = session.query(TwitterTrend).filter(TwitterTrend.query == "%23OwnYourCredit").one()
	if top_trends:
		for trend in top_trends:
			# get last tweet for this trend
			last_tweet = session.query(TrendTweet).filter(TrendTweet.trend_id == trend.id).order_by(TrendTweet.tweeted_at.desc()).first()
			if last_tweet:
				fetch_trends_tweets(trend, since_id=last_tweet.tweet_id)
			else:
				fetch_trends_tweets(trend)
	# fetch_trends_tweets(top_trends)
