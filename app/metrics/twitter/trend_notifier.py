from ..twitter_config import get_db_session
from ...models import TwitterTrend
from ...email import send_email
from ... import create_app
import os
session = get_db_session()


def notify():
	top_trends = session.query(TwitterTrend).filter(TwitterTrend.trending == True, TwitterTrend.position < 11).order_by(TwitterTrend.position)
	new_trends = []
	for trend in top_trends:
		if not trend.prev_position or trend.prev_position > 10:
			new_trends.append(trend.name)
			print "{} has just started trending".format(trend.name)
	if len(new_trends) > 0:
		to = 'info@datametrics.co.ke'
		subject = "NEW TREND ALERT"
		template = "email/trend_alert"
		send_email(to, subject, template, trends=new_trends)


if __name__ == '__main__':
	app = create_app(os.getenv('FLASK_CONFIG') or 'default')
	with app.app_context():
		notify()
