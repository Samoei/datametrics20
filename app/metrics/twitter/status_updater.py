from ..twitter_config import get_twitter_api, get_db_session
from ...models import TwitterTrend, ScheduledTopTrending
import time
import sys
from tweepy import TweepError

api = get_twitter_api()
session = get_db_session()


def update_status(status):
	if status:
		try:
			sys.stderr.write("TRYING TO UPDATE STATUS:\n {}\n".format(status))
			api.update_status(status)
		except TweepError as e:
			sys.stderr.write("ERROR {}\n".format(e))
			time.sleep(30)



def tweet_trending_trends(trends):
	status = ""
	count = 1
	new_trends = ["TOP 5 TRENDING NOW",]
	for trend in trends:
		status = "".join([str(count),".",trend.name])
		new_trends.append(status)
		count = count + 1
	new_trends.append("http://bit.ly/2hT5yNk")
	# print new_trends
	return "\n".join(new_trends)


def get_trending_trends():
	top_trends = session.query(TwitterTrend).filter(TwitterTrend.trending == True, TwitterTrend.position < 6).order_by(TwitterTrend.position)
	return top_trends

def save_currently_trending(status):
	tweet = ScheduledTopTrending()
	tweet.tweet_text = status
	session.add(tweet)
	try:
		session.commit()
	except:
		session.rollback()


if __name__ == '__main__':
	trends = get_trending_trends()
	status = tweet_trending_trends(trends)
	# update_status(status)
	save_currently_trending(status)
