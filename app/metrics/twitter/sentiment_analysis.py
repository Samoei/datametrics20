from ...models import *
from ..twitter_config import get_db_session
from textblob import TextBlob
from sqlalchemy.exc import IntegrityError
session = get_db_session()
terms = ['raila', 'odinga', 'uhuru', 'kenyatta']


def process_tweets(tweets):
	for tweet in tweets:
		status = tweet.text
		status = status.lower()
		if "raila" in status or "odinga" in status:
			searchterm = "raila"
		elif "uhuru" in status or "kenyatta" in status:
			searchterm = "uhuru"
		analysed_tweet = TextBlob(status)
		print "POLARITY", analysed_tweet.sentiment.polarity
		print "SUBJECTIVITY", analysed_tweet.sentiment.subjectivity

		if analysed_tweet.sentiment.polarity < 0:
			sentiment = "negative"
		elif analysed_tweet.sentiment.polarity == 0:
			sentiment = "neutral"
		else:
			sentiment = "positive"

		# 0.0 is very objective and 1.0 is very subjective
		if analysed_tweet.sentiment.subjectivity < 0.6:
			subjective = "objective"
		else:
			subjective = "subjective"

		tweet.polarity_desc = sentiment
		tweet.polarity_value = analysed_tweet.sentiment.polarity
		tweet.subjectivity_value = analysed_tweet.sentiment.subjectivity
		tweet.subjectivity_desc = subjective
		tweet.processed = True
		tweet.searchterm = searchterm

		# session.add(tweet)
		# session.commit()

		try:
			session.commit()
		except IntegrityError:
			session.rollback()
		except:
			session.rollback()


if __name__ == '__main__':
	tweets = session.query(StreamedTweets).filter(StreamedTweets.processed != False).all()
	process_tweets(tweets)
