from ..twitter_config import get_twitter_api, get_db_session
from ...models import TwitterUser, TwitterUserMeta
import tweepy
from datetime import datetime
from sqlalchemy.orm.exc import NoResultFound

api = get_twitter_api()
session = get_db_session()
def get_user():
	for friend in tweepy.Cursor(api.friends).items(500):
		name = friend.name
		twitter_user_id = friend.id_str
		screen_name = friend.screen_name
		followers_count = friend.followers_count
		friends_count = friend.friends_count
		status_count = friend.statuses_count
		listed_count = friend.listed_count
		if friend.description:
			description = friend.description
		else:
			description = ""
		favourites_count = friend.favourites_count
		location = friend.location
		verified = friend.verified
		created_at = friend.created_at
		tracking_start = datetime.utcnow()
		lang = friend.lang
		profile_img_url = (friend.profile_image_url).replace("_normal", "")


		try:
			user = session.query(TwitterUser).filter(TwitterUser.screen_name == screen_name).one()
			# UPDATE USER DETAILS
			update_user_details(user, name, followers_count,friends_count,status_count, listed_count, favourites_count, location, verified, description, profile_img_url)
			create_user_meta(user.id, followers_count, friends_count, status_count, listed_count, favourites_count)
		except NoResultFound, e:
			user = create_user(name, twitter_user_id, screen_name, followers_count,friends_count,status_count, listed_count, favourites_count, location, verified, description, created_at, lang, profile_img_url)
			create_user_meta(user.id, followers_count, friends_count, status_count, listed_count, favourites_count)
		finally:
			session.commit()


def update_user_details(user, name, followers_count,friends_count,status_count, listed_count, favourites_count, location, verified, description, profile_img_url):
	user.name = name
	user.followers_count = followers_count
	user.friends_count = friends_count
	user.status_count = status_count
	user.listed_count = listed_count
	user.favourites_count = favourites_count
	user.location = location
	user.verified = verified
	user.description = description
	user.profile_img_url = profile_img_url
	user.last_updated = datetime.utcnow()
	session.add(user)

def create_user(name, twitter_user_id, screen_name, followers_count,friends_count,status_count, listed_count, favourites_count, location, verified, description, created_at, lang, profile_img_url):
	user = TwitterUser()
	user.name = name
	user.twitter_user_id = twitter_user_id
	user.screen_name = screen_name
	user.created_at = created_at
	user.lang = lang
	user.followers_count = followers_count
	user.friends_count = friends_count
	user.status_count = status_count
	user.listed_count = listed_count
	user.favourites_count = favourites_count
	user.location = location
	user.verified = verified
	user.description = description
	user.profile_img_url = profile_img_url
	user.tracking_start = datetime.utcnow()
	session.add(user)

	return user

def create_user_meta(user_id, followers_count, friends_count, status_count, listed_count, favourites_count):
	user_meta = TwitterUserMeta()
	user_meta.followers_count = followers_count
	user_meta.friends_count = friends_count
	user_meta.status_count = status_count
	user_meta.listed_count = listed_count
	user_meta.favourites_count = favourites_count
	user_meta.twitter_user_id = user_id
	session.add(user_meta)





if __name__ == '__main__':
  get_user()