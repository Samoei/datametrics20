from ..twitter_config import get_twitter_api, get_db_session
from ...models import TwitterTrend
from datetime import datetime
from sqlalchemy.orm.exc import NoResultFound

api = get_twitter_api()
session = get_db_session()

def process_twitter_trends():
  current_db_trends = get_trends()
  db_trend_list = []
  for db_trend in current_db_trends:
    db_trend_list.append(db_trend.name)
  api_trends = get_twitter_trends(23424863) # KENYA
  # api_trends = get_twitter_trends(1528488) # NAIROBI
  position = 1
  current_trends = []
  old_trends = []
  for item in api_trends[0]['trends']:
    try:
      trend = session.query(TwitterTrend).filter(TwitterTrend.name == item['name'], TwitterTrend.trending == True).one()
      # TRENDING UPDATE DETAILS
      trend.updated_date = datetime.utcnow()
      trend.prev_position = trend.position
      trend.position = position
      session.add(trend)
      current_trends.append(item['name'])
      create_trend_meta(trend.id, trend.position)
      position = position + 1
    except NoResultFound, e:
      # NOT TRENDING CREATE A NEW RECORD HERE
      trend = TwitterTrend()
      trend.name = item['name']
      trend.trending = True
      trend.started = datetime.utcnow()
      trend.updated_date = datetime.utcnow()
      trend.position = position
      trend.query = item['query']
      trend.url = item['url']
      session.add(trend)
      create_trend_meta(trend.id, trend.position)
      position = position + 1
    # REMOVE ANY OLD TREND
  session.commit()
  update_db_trends(current_trends, db_trend_list)



def update_db_trends(current_trends, db_trend_list):
  old_trends = get_new_trends(db_trend_list, current_trends)
  for trnd in old_trends:
    try:
      db_trnd = session.query(TwitterTrend).filter(TwitterTrend.name == trnd, TwitterTrend.trending == True).one()
      db_trnd.trending=False
      db_trnd.ended=datetime.utcnow()
      session.add(db_trnd)
    except NoResultFound, e:
      pass
    finally:
      session.commit()

def create_trend_meta(trend_id, trend_position):
  pass

def get_trends():
  return session.query(TwitterTrend).filter(TwitterTrend.trending == True).all()

def get_twitter_trends(location):
  return api.trends_place(location)

def get_common_trends(a, b):
  return set(a) & set(b)

def get_new_trends(a, b):
  return [item for item in a if item not in b]


def get_locations_available():
  result = api.trends_available()
  for item in result:
    print item

if __name__ == '__main__':
  process_twitter_trends()
