import sys
import time
import tweepy
from ...models import *
from ..twitter_config import get_twitter_api, get_db_session
from sqlalchemy.exc import IntegrityError

api = get_twitter_api()
session = get_db_session()

# keyword_list = ['Raila', 'Odinga', 'Uhuru', 'Kenyatta', 'William Ruto', 'DP Rutto', 'Kalonzo musyoka', 'Musalia Mudavadi', 'Moses Wetangula', 'jubilee', 'Cord', 'ODM', 'Uhuru', 'Kenyatta', 'ODM Party', '@TheODMparty', '@JubileePartyK', '#TukoPamoja', '#UhuruTena'
# ]

keyword_list = ['raila', 'raila odinga', 'odinga', 'uhuru', 'kenyatta', 'uhuru kenyatta', 'william ruto', 'dp ruto']


def add_keyword():
	pass


def add_search_term(keyword_id):
	pass


# override tweepy.StreamListener to add logic to on_status
class PoliticalStreamListener(tweepy.StreamListener):
	def on_status(self, status):
		try:
			try:
				if status.retweeted_status:
					print "THIS IS A RETWEET, SKIPPING..."
					return
			except:
				store_tweets(status)
		except BaseException as e:
			sys.stderr.write("Error on data: {}\n".format(e))
			time.sleep(20)
			return True

	def on_error(self, status_code):
		if status_code == 420:
			sys.stderr.write("Rate Limit Exceeded\n")
			return False
		else:
			sys.stderr.write("Error {}\n".format(status_code))
			return True

	def on_timeout(self):
		sys.stderr.write("TIMEOUT Error...")
		time.sleep(30)
		return True  # Don't kill the stream


def store_tweets(status):
	tweet = StreamedTweets()
	tweet.text = (status.text).encode('utf-8').strip()
	tweet.tweet_id = status.id_str
	tweet.tweeted_at = status.created_at
	print "Saving Tweet: \n{}".format((status.text).encode('utf-8').strip())
	if status.retweet_count:
		tweet.retweet_count = status.retweet_count
	if status.favorite_count:
		tweet.favourite_count = status.favorite_count
	if status.user.screen_name:
		tweet.handle = status.user.screen_name
	if status.user.profile_image_url:
		tweet.profile_img_url = status.user.profile_image_url.replace("_normal", "")
	if status.user.name:
		tweet.name = status.user.name.encode('utf-8').strip()
	if status.source:
		tweet.tweeted_using = status.source.encode('utf-8').strip()
	if status.user.friends_count:
		tweet.friends_count = status.user.friends_count
	if status.user.followers_count:
		tweet.followers_count = status.user.followers_count
	if status.user.statuses_count:
		tweet.statuses_count = status.user.statuses_count

	session.add(tweet)
	try:
		session.commit()
	except IntegrityError:
		session.rollback()
	except:
		session.rollback()



def start_stream():
	while True:
		try:
			myStream = tweepy.Stream(auth=api.auth, listener=PoliticalStreamListener())
			myStream.filter(track=keyword_list)
			# myStream.filter(locations=[34, -5.3, 43.5, 5.61])
		except KeyboardInterrupt:
			sys.stderr.write("Political Stream Exiting....\nGood Bye\n")
			sys.exit(1)
		except:
			sys.stderr.write("Political Stream Sleeping for 120 seconds....\nGood Night\n")
			time.sleep(120)
			sys.stderr.write("Political Stream Has just woken up....\nStill here...Good Morning\n")
			continue


if __name__ == '__main__':
	start_stream()
