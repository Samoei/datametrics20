from flask import Flask
from config import config, Config

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager
from flask.ext.pagedown import PageDown
from flask.ext.moment import Moment
from flask.ext.mail import Mail
from flask.ext.cache import Cache

from celery import Celery


# Initializations
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
db = SQLAlchemy()
bootstrap = Bootstrap()
pagedown = PageDown()
moment = Moment()
mail = Mail()
# cache = MemcachedCache(['127.0.0.1:11211'])
cache = Cache(config={'CACHE_TYPE': 'memcached', 'CACHE_MEMCACHED_SERVERS': (['127.0.0.1:11211'])})

celery = Celery(__name__, broker=Config.CELERY_BROKER_URL)


def create_app(config_name):
	app = Flask(__name__)
	app.config.from_object(config[config_name])

	login_manager.init_app(app)
	bootstrap.init_app(app)
	db.init_app(app)
	pagedown.init_app(app)
	moment.init_app(app)
	mail.init_app(app)
	cache.init_app(app)
	celery.conf.update(app.config)

	from .main import main as main_blueprint
	from .auth import auth as auth_blueprint
	from .profile import profile as profile_blueprint
	from .blog import blog as blog_blueprint
	from .metrics import metrics as metrics_blueprint
	from .dashboard import dashboard as dashboard_blueprint
	from .demo import demo as demo_blueprint
	app.register_blueprint(main_blueprint)
	app.register_blueprint(auth_blueprint, url_prefix='/auth')
	app.register_blueprint(profile_blueprint)
	app.register_blueprint(blog_blueprint, url_prefix='/blog')
	app.register_blueprint(metrics_blueprint, url_prefix='/metrics')
	app.register_blueprint(dashboard_blueprint, url_prefix='/dashboard')
	app.register_blueprint(demo_blueprint, url_prefix='/demo')

	return app
