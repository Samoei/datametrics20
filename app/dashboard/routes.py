from flask import render_template, jsonify
from . import dashboard
from ..models import StreamedTweets
from .. import db


@dashboard.route('/sentiment')
def scheduled_tweet():
	return render_template('dashboard/sentiment.html')


@dashboard.route("/sentiment/tweets")
def sentiment_tweets():
	return jsonify(json_list=[i.serialize for i in db.session.query(StreamedTweets).filter(StreamedTweets.processed == True).all()])
