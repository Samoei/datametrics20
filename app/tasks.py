from . import celery
from . import mail


@celery.task
def send_async_email(msg):
	mail.send(msg)
