// loader settings
var opts = {
  lines: 13 // The number of lines to draw
, length: 28 // The length of each line
, width: 14 // The line thickness
, radius: 42 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
}
var target = document.getElementById('#popularity-ring-chart');

var spinner = new Spinner(opts).spin(target);
queue()
    .defer(d3.json, "/dashboard/sentiment/tweets")
    .await(makeGraphs);
spinner.stop();

function makeGraphs(error, json_list, statesJson) {

	var tweetsObject = json_list.json_list;

	var dateFormat = d3.time.format("%Y-%m-%d %H:%M:%S");


	tweetsObject.forEach(function(d) {
		d["tweeted_at"] = dateFormat.parse(d["tweeted_at"]);
	});

	//Create a Crossfilter instance
	var ndx = crossfilter(tweetsObject);

	//Define Dimensions
	var dateDim = ndx.dimension(function(d) { return d["tweeted_at"]; });
	var handleDim = ndx.dimension(function(d) { return d["handle"]; });
	var polarityDim = ndx.dimension(function(d) { return d["polarity_desc"]; });
	var subjectivityDim = ndx.dimension(function(d) { return d["subjectivity_desc"]; });
	var appDim  = ndx.dimension(function(d) { return d["tweeted_using"]; });
	var searchTermDim  = ndx.dimension(function(d) { return d["searchterm"]; });

	var totalTweets = ndx.groupAll().reduceCount().value();
	// var totalTweetsForRails = ndx.groupAll().reduceCount(function (fact) {
	// 	return fact.searchterm;
	// }).value();

	console.log("TOTAL TWEETS", totalTweets)

	//Calculate metrics
	tweetsByHandle = dateDim.group();
	tweetsByPolarity = polarityDim.group();
	tweetsBySubjectivity = subjectivityDim.group();
	railaPolarity = polarityDim.group(function (d) {
		return
	});
	tweetsByApp = appDim.group();
	tweetsBySearchTerm = searchTermDim.group();

	var allTweets = ndx.groupAll();


	//Draw the charts

	var popularityRingChart = dc.pieChart("#popularity-ring-chart");
      popularityRingChart
      	.width(250).height(250)
            .dimension(searchTermDim)
            .group(tweetsBySearchTerm)
            .innerRadius(50);


	var polarityChart = dc.rowChart("#polarity-row-chart");
	polarityChart
        .width(300)
        .height(250)
        .dimension(polarityDim)
        .group(tweetsByPolarity)
        .xAxis().ticks(4);

     var searchTermChart = dc.rowChart("#search-term-row-chart");
     searchTermChart
		.width(300)
		.height(200)
       	.dimension(searchTermDim)
      	.group(tweetsBySearchTerm)
      	.xAxis().ticks(4);


	dc.renderAll();

}