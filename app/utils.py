def percentage(part, whole):
	if whole == 0:
		return 0.0
	return 100 * float(part) / float(whole)
