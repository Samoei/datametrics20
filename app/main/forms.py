from flask.ext.wtf import Form
from wtforms import StringField
from wtforms.validators import Required, Length, Email
from wtforms.widgets import TextArea


class ContactForm(Form):
	name = StringField('Your name', validators=[Required(), Length(7, 64, "Atleast Two Names required")],)
	email = StringField('Email', validators=[Required(), Length(5, 64), Email()])
	body = StringField('Message', widget=TextArea(), validators=[Required()])
