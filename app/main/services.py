from ..metrics.twitter_config import get_db_session
from ..models import TwitterTrend, TrendTweet, TrendUserActivity
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound

session = get_db_session()


def update_trend_user_details(trend, user_tweets, user_retweets):
	for item in user_tweets:
		try:
			user = session.query(TrendUserActivity).filter(TrendUserActivity.trend_id == trend.id, TrendUserActivity.screen_name == item[1]).one()
			# UPDATE USER DETAILS
			user.tweets = item[0]
			session.add(user)
		except NoResultFound:
			user = TrendUserActivity()
			user.tweets = item[0]
			user.screen_name = item[1]
			user.trend_id = trend.id
			session.add(user)
		finally:
			session.commit()
	for item in user_retweets:
		try:
			user = session.query(TrendUserActivity).filter(TrendUserActivity.trend_id == trend.id, TrendUserActivity.screen_name == item[1]).one()
			# UPDATE USER DETAILS
			user.retweets = item[0]
			session.add(user)
		except NoResultFound:
			user = TrendUserActivity()
			user.retweets = item[0]
			user.screen_name = item[1]
			user.trend_id = trend.id
			session.add(user)
		finally:
			session.commit()


def get_active_for_trend(trend):
	user_tweets = session.query(func.count(TrendTweet.tweeted_by), TrendTweet.tweeted_by).group_by(TrendTweet.tweeted_by).filter(TrendTweet.trend_id == trend.id, TrendTweet.retweet == None).order_by(func.count(TrendTweet.tweeted_by).desc()).all()

	user_retweets = session.query(func.count(TrendTweet.tweeted_by), TrendTweet.tweeted_by).group_by(TrendTweet.tweeted_by).filter(TrendTweet.trend_id == trend.id, TrendTweet.retweet == True).order_by(func.count(TrendTweet.tweeted_by).desc()).all()

	update_trend_user_details(trend, user_tweets, user_retweets)


if __name__ == '__main__':
	top_trends = session.query(TwitterTrend).filter(TwitterTrend.trending == True, TwitterTrend.position < 6)
	for trend in top_trends:
		get_active_for_trend(trend)
