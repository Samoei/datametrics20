from ..metrics.twitter_config import get_db_session
from ..models import TwitterTrend, TrendTweet, TrendUserActivity, TrendAppUsage, TrendTopTweets
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from datetime import datetime, timedelta
from app.metrics.twitter.trends_summary_updater import tweet_active_retweeters, tweet_active_tweeters
import time

session = get_db_session()
# Get all trends with no summary
def get_trends():
	half_day_ago = datetime.today() - timedelta(days=2)
	trends = session.query(TwitterTrend).filter(TwitterTrend.started > half_day_ago, TwitterTrend.ended != None,  TwitterTrend.summarized == None).all()
	return trends


def summarize_trends(trend, tweets):
	populate_top_tweets(trend, tweets)
	populate_app_usage(trend, tweets)
	populate_user_activity(trend)
	populate_trend_numbers(trend, tweets)
	tweet_active_tweeters(trend)
	tweet_active_retweeters(trend)




def populate_top_tweets(trend, tweets):
	# GET TWEEST THAT ARE NOT OLDER THAN 15 MINUTES SINCE TREND START
	query_date = trend.started - timedelta(minutes=30)
	top_tweets = tweets.filter(TrendTweet.retweet == None, TrendTweet.tweeted_at > query_date).order_by(TrendTweet.retweet_count.desc())[:10]
	print "#################### TREND TOP TWEETS ####################"
	for tweet in top_tweets:
		top_tweet = TrendTopTweets()
		top_tweet.trend_id = trend.id
		top_tweet.text = tweet.text
		top_tweet.tweet_id = tweet.tweet_id
		top_tweet.tweeted_at = tweet.tweeted_at
		top_tweet.retweet_count = tweet.retweet_count
		top_tweet.favourite_count = tweet.favourite_count
		top_tweet.handle = tweet.tweeted_by
		top_tweet.name = tweet.user_name
		top_tweet.tweeted_using = tweet.tweeted_using
		top_tweet.source_url = tweet.source_url
		top_tweet.tweeted_from = tweet.tweeted_from
		top_tweet.friends_count = tweet.friends_count
		top_tweet.followers_count = tweet.followers_count
		top_tweet.statuses_count = tweet.statuses_count
		top_tweet.verified = tweet.verified
		top_tweet.profile_img_url = tweet.profile_img_url
		session.add(top_tweet)
	try:
		session.commit()
	except IntegrityError:
		pass
	except:
		session.rollback()
	print "\n"


def populate_app_usage(trend, tweets):
	devices = session.query(func.count(TrendTweet.tweeted_using), TrendTweet.tweeted_using).group_by(TrendTweet.tweeted_using).filter(TrendTweet.trend_id == trend.id).order_by(func.count(TrendTweet.tweeted_using).desc())[:10]
	print "#################### TREND APP USAGE ####################"
	for item in devices:
		app_usage = TrendAppUsage()
		app_usage.app_name = item[1]
		app_usage.count = item[0]
		app_usage.trend_id = trend.id
		session.add(app_usage)
		session.commit()
		# print"{}: {}".format(item[1], item[0])
	print "\n"


def populate_user_activity(trend):
	user_tweets = session.query(func.count(TrendTweet.tweeted_by), TrendTweet.tweeted_by).group_by(TrendTweet.tweeted_by).filter(TrendTweet.trend_id == trend.id, TrendTweet.retweet == None).order_by(func.count(TrendTweet.tweeted_by).desc())[:10]

	user_retweets = session.query(func.count(TrendTweet.tweeted_by), TrendTweet.tweeted_by).group_by(TrendTweet.tweeted_by).filter(TrendTweet.trend_id == trend.id, TrendTweet.retweet == True).order_by(func.count(TrendTweet.tweeted_by).desc())[:10]
	print "#################### TREND USER ACTIVITY ####################"
	print "FETHED {} TWEETS & {} RETWEETS".format(len(user_tweets), len(user_retweets))
	populate_user_retweets(trend, user_retweets)
	populate_user_tweets(trend, user_tweets)
	print "\n"


def populate_user_retweets(trend, user_retweets):
	print "#################### POPULATING RETWEETS ####################"
	for item in user_retweets:
		print "WORKING ON USER: {} WITH RETWEETS: {}".format(item[1], item[0])
		user = TrendUserActivity()
		user.retweets = item[0]
		user.screen_name = item[1]
		user.trend_id = trend.id
		session.add(user)
	try:
		session.commit()
	except IntegrityError:
		pass
	except:
		session.rollback()


def populate_user_tweets(trend, user_tweets):
	print "#################### POPULATING TWEETS ####################"
	for item in user_tweets:
		print "WORKING ON USER: {} WITH TWEETS: {}".format(item[1], item[0])
		# Check if current user exists
		try:
			user = session.query(TrendUserActivity).filter(TrendUserActivity.trend_id == trend.id, TrendUserActivity.screen_name == item[1]).one()
			# UPDATE USER DETAILS
			user.tweets = item[0]
			session.add(user)
		except NoResultFound:
			user = TrendUserActivity()
			user.tweets = item[0]
			user.screen_name = item[1]
			user.trend_id = trend.id
			session.add(user)
		finally:
			try:
				session.commit()
			except IntegrityError:
				pass
			except:
				session.rollback()





def populate_trend_numbers(trend, tweets):
	total_tweets = tweets.filter(TrendTweet.retweet == None).count()
	total_retweets = tweets.filter(TrendTweet.retweet == True).count()
	total_users = session.query(TrendTweet.tweeted_by).distinct().filter(TrendTweet.trend_id == trend.id).count()
	print "#################### TREND NUMBERS ####################"
	trend.total_tweets = total_tweets
	trend.total_retweets = total_retweets
	trend.total_users = total_users
	session.add(trend)
	session.commit()


if __name__ == '__main__':
	trends = get_trends()
	print "FETCHED {} TRENDS: ".format(len(trends))
	for trend in trends:
		tweets = trend.tweets
		if tweets.count() > 0:
			print "WORKING ON TREND {}, STARTED AT {} AND ENDED AT {}".format(trend.name, trend.started, trend.ended)
			print "TWEETS COUNT: ", tweets.count()
			summarize_trends(trend, tweets)
		trend.summarized = True
		session.add(trend)
		session.commit()