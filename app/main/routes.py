from flask import render_template, flash, redirect, url_for
from ..email import send_email
from . import main
from ..models import BlogPost
from ..models import TwitterTrend, TwitterUser, TrendTweet, TrendUserActivity, TrendAppUsage, TrendTopTweets, ScheduledTweets, ScheduledTopTrending
from sqlalchemy import func
from .. import db
from .. import cache
# from flask.ext.cache.Cache import cache
from .forms import ContactForm

from bokeh.charts import Bar, Donut
from bokeh.resources import INLINE
from bokeh.embed import components
import pandas as pd
from bokeh.util.string import encode_utf8
from ..utils import percentage
from flask.ext.login import login_required
from datetime import datetime


@main.route('/')
def index():
	return render_template('main/index.html')


@main.route('/about')
def about():
	return render_template('main/about.html')


@main.route('/scheduled-tweet/<int:id>',)
@login_required
def sheduled_tweet(id):
	tweet = db.session.query(ScheduledTweets).filter(ScheduledTweets.id == id).one()
	tweet.posted = True
	db.session.add(tweet)
	db.session.commit()
	return redirect(url_for('main.scheduled_tweet'))


@main.route('/scheduled-toptrending/<int:id>',)
@login_required
def sheduled_toptrending(id):
	tweet = db.session.query(ScheduledTopTrending).filter(ScheduledTopTrending.id == id).one()
	tweet.posted = True
	db.session.add(tweet)
	db.session.commit()
	return redirect(url_for('main.scheduled_tweet'))


@main.route('/scheduled-tweets', methods=['GET', 'POST'])
@login_required
def scheduled_tweet():
	tweets = db.session.query(ScheduledTweets).filter(ScheduledTweets.posted == False).order_by(ScheduledTweets.generated_on.desc()).all()
	top_trends = db.session.query(ScheduledTopTrending).filter(ScheduledTopTrending.posted == False).order_by(ScheduledTopTrending.generated_on.desc()).all()
	return render_template('main/scheduled_tweets.html', tweets=tweets, top_trends=top_trends)


@main.route('/contact', methods=['GET', 'POST'])
def contact():
	form = ContactForm()
	if form.validate_on_submit():
		name = form.name.data
		email = form.email.data
		body = form.body.data
		subject = "CONTACT US FROM {}, EMAIL: {}".format(name, email)
		to = 'info@datametrics.co.ke'
		send_email(to, subject, "email/contact_us", name=name, message=body)
		flash('Thank you {} for your message.\bWe shall get back to you shortly'.format(name), 'info')
		return redirect(url_for('main.contact'))
	return render_template('main/contact.html', form=form)

@main.route('/faq')
def faq():
	return render_template('main/fag.html')

@main.route('/blog')
def blog():
	posts = BlogPost.query.order_by(BlogPost.draft_date.desc())
	return render_template('main/blog.html', posts=posts)


@main.route('/twitter')
def twitter():
	top_users = db.session.query(TwitterUser).order_by(TwitterUser.followers_count.desc())[:10]
	top_trends = db.session.query(TwitterTrend).filter(TwitterTrend.trending == True).order_by(TwitterTrend.position)[:10]
	today_midnight = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
	recently_trending = db.session.query(TwitterTrend).filter(TwitterTrend.total_retweets != None, TwitterTrend.ended> today_midnight).order_by(TwitterTrend.ended.desc())[:15]
	return render_template('main/twitter-home.html', top_trends=top_trends, top_users=top_users, recently_trending=recently_trending)


@main.route('/twitter/trend/<int:id>/')
@cache.memoize(timeout=10000)
def trend_details(id):
	trend = db.session.query(TwitterTrend).filter(TwitterTrend.id == id).one()
	total_tweets = trend.total_tweets + trend.total_retweets
	tweets_percentage = percentage(trend.total_tweets, total_tweets)
	retweets_percentage = percentage(trend.total_retweets, total_tweets)
	apps_usage = trend.apps.order_by(TrendAppUsage.count.desc())[:5]
	top_tweets = trend.top_tweets.order_by(TrendTopTweets.retweet_count.desc())[:10]
	users_activity = trend.users_activity
	users_tweets = users_activity.filter(TrendUserActivity.tweets != None).order_by(TrendUserActivity.tweets.desc())[:10]
	users_retweets = users_activity.filter(TrendUserActivity.retweets != None).order_by(TrendUserActivity.retweets.desc())[:10]

	apps_usage_list = []
	for app in apps_usage:
		app_usage = (app.app_name, app.count)
		apps_usage_list.append(app_usage)
	df = pd.DataFrame(apps_usage_list, columns=['app_name', 'count'])
	p = Bar(df, label='app_name', values='count', color='#1ABB9C', responsive=True)
	p.logo = None
	p.toolbar_location = None
	apps_usage_script, apps_usage_div = components(p, INLINE)

	data = pd.Series([trend.total_tweets, trend.total_retweets], index=['tweets', 'retweets'])
	pie_chart = Donut(data, responsive=True)
	pie_chart.logo = None
	pie_chart.toolbar_location = None

	pie_script, pie_div = components(pie_chart, INLINE)



	css_resources = INLINE.render_css()
	js_resources = INLINE.render_js()

	html =  render_template('main/trend_detail.html', trend=trend, js_resources=js_resources, css_resources=css_resources,  tweets_percentage=tweets_percentage, retweets_percentage=retweets_percentage, apps_usage_script=apps_usage_script, apps_usage_div=apps_usage_div, pie_script=pie_script, pie_div=pie_div, top_tweets=top_tweets, users_retweets=users_retweets, users_tweets=users_tweets)

	return encode_utf8(html)


@main.route('/twitter/trend/<int:id>/<order>')
# @cache.memoize(timeout=300)
def trend(id, order='latest'):
	trend = db.session.query(TwitterTrend).filter(TwitterTrend.id == id).one()
	# trend_users_tweets = db.session.query(TrendUserActivity).filter(TrendUserActivity.trend_id == id, TrendUserActivity.tweets != None).order_by(TrendUserActivity.tweets.desc())[:10]
	# trend_users_retweets = db.session.query(TrendUserActivity).filter(TrendUserActivity.trend_id == id, TrendUserActivity.retweets != None).order_by(TrendUserActivity.retweets.desc())[:10]
	tweets = trend.tweets
	users_activity = trend.users_activity
	trend_users_tweets = users_activity.filter(TrendUserActivity.tweets != None).order_by(TrendUserActivity.tweets.desc())[:10]
	trend_users_retweets = users_activity.filter(TrendUserActivity.retweets != None).order_by(TrendUserActivity.retweets.desc())[:10]
	total_users = db.session.query(TrendTweet.tweeted_by).distinct().filter(TrendTweet.trend_id == id).count()
	devices = db.session.query(func.count(TrendTweet.tweeted_using), TrendTweet.tweeted_using).group_by(TrendTweet.tweeted_using).filter(TrendTweet.trend_id == trend.id).order_by(func.count(TrendTweet.tweeted_using).desc())[:5]
	print devices
	# influencers = db.session.query(TrendTweet.tweeted_by, TrendTweet.followers_count, TrendTweet.friends_count, TrendTweet.user_name, TrendTweet.statuses_count, TrendTweet.profile_img_url).distinct(TrendTweet.tweeted_by).filter(TrendTweet.trend_id == id).all()

	script = ""
	div = ""
	css_resources = INLINE.render_css()
	js_resources = INLINE.render_js()
	if len(devices) > 0:
		df = pd.DataFrame(devices, columns=['Count', 'Source'])
		p = Bar(df, label='Source', values='Count', color='#1ABB9C', responsive=True)
		p.logo = None
		p.toolbar_location = None

		script, div = components(p, INLINE)

	total_tweets = tweets.filter(TrendTweet.retweet == None).count()
	retweets = tweets.filter(TrendTweet.retweet == True).count()

	tweets_percentage = percentage(total_tweets, tweets.count())
	retweets_percentage = percentage(retweets, tweets.count())



	data = pd.Series([total_tweets, retweets], index=['tweets', 'retweets'])
	pie_chart = Donut(data, responsive=True)
	pie_chart.logo = None
	pie_chart.toolbar_location = None

	pie_script, pie_div = components(pie_chart, INLINE)

	top_tweets = None
	if order == 'retweets':
		top_tweets = tweets.filter(TrendTweet.retweet == None).order_by(TrendTweet.retweet_count.desc())[:5]
	elif order == 'likes':
		top_tweets = tweets.filter(TrendTweet.retweet == None).order_by(TrendTweet.favourite_count.desc())[:5]
	elif order == 'latest':
		top_tweets = tweets.filter(TrendTweet.retweet == None).order_by(TrendTweet.tweeted_at.desc())[:5]
	elif order == 'oldest':
		top_tweets = tweets.filter(TrendTweet.retweet == None).order_by(TrendTweet.tweeted_at)[:5]
	html =  render_template('main/trend.html', trend=trend, total_tweets=total_tweets, devices=devices, total_users=total_users, top_tweets=top_tweets, order=order, script=script, div=div, js_resources=js_resources, css_resources=css_resources, retweets=retweets, pie_script=pie_script, pie_div=pie_div, tweets_percentage=tweets_percentage, retweets_percentage=retweets_percentage, trend_users_tweets=trend_users_tweets, trend_users_retweets=trend_users_retweets)

	return encode_utf8(html)


@main.app_errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404


@main.app_errorhandler(500)
def internal_server_error(e):
	return render_template('500.html'), 500
