from flask.ext.wtf import Form
from wtforms import StringField, TextField
from wtforms.validators import Required, Length


class ProfileFrom(Form):
    first_name = StringField('First Name', validators=[Required(), Length(1, 64)])
    last_name = StringField('Last Name', validators=[Required(), Length(1, 64)])
    location = StringField('Current Location', validators=[Length(4, 64)])
    bio = TextField('Current Location')

















