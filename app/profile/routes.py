from flask import render_template, flash, redirect, url_for
from flask.ext.login import login_required, current_user
from . import profile
from .forms import ProfileFrom
from ..models import User
from .. import db

@profile.route('/user/edit', methods=['GET', 'POST'])
def edit():
  form = ProfileFrom()
  if form.validate_on_submit():
    current_user.first_name = form.first_name.data
    current_user.last_name = form.last_name.data
    current_user.location = form.location.data
    current_user.bio = form.bio.data
    db.session.add(current_user._get_current_object())
    db.session.commit()
    flash('You profile has been updated')
    return redirect(url_for('profile.profile', username=current_user.username))
  form.first_name.data = current_user.first_name
  form.last_name.data = current_user.last_name
  form.location.data = current_user.location
  form.bio.data = current_user.bio
  return render_template('/profile/edit-profile.html', form = form, user=current_user)


@profile.route('/user/<username>')
def profile(username):
  user = User.query.filter_by(username=username).first_or_404()
  return render_template('/profile/profile.html', user = user)

