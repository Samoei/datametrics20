from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField, SubmitField
from wtforms.validators import Required, Length, Email, Optional, URL,Regexp
from wtforms.widgets import TextArea
from werkzeug.utils import secure_filename
from flask_wtf.file import FileField, FileAllowed, FileRequired
# from flask_pagedown.fields import PageDownField
from flask.ext.pagedown.fields import PageDownField


class PostForm(Form):
	title = StringField('Title', validators=[Required(), Length(1, 64)])
	body = PageDownField('Body', validators=[Required()])
	posted = BooleanField('Post')
	featured_image = FileField('featured_image', validators=[FileAllowed(['jpg', 'png'], 'Upload Images only!')])
	video = StringField('Video Link', validators=[Optional(), URL()])


class CommentForm(Form):
	name = StringField('Your Name', validators=[Required(), Length(7, 64, "Atleast Two Names required")],)
	email = StringField('Your Email', validators=[Required(), Length(5, 64), Email()])
	body = PageDownField('Your Message', validators=[Required()])
	notify = BooleanField('Notify me when new comments are posted for this post', default=True)
