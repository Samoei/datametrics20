from flask import render_template, redirect, url_for, current_app, flash
from flask.ext.login import current_user
from werkzeug.utils import secure_filename
from . import blog
from .forms import PostForm, CommentForm
from ..models import BlogPost, BlogComments
from .. import db
import os
import uuid
import re
from datetime import datetime


@blog.route('/new', methods=['POST', 'GET'])
def new_post():
	form = PostForm()
	if form.validate_on_submit():
		filename = secure_filename(form.featured_image.data.filename)
		extension = filename.rsplit('.', 1)[1]
		featured_image = str(uuid.uuid4()) + '.' + extension
		form.featured_image.data.save(os.path.join(current_app.config['UPLOAD_FOLDER'], featured_image))
		title = form.title.data
		body = form.body.data
		posted = form.posted.data
		video = form.video.data
		slug = re.sub('[^\w]+', '-', title.lower()).strip('-')
		publish_date = None
		if posted is True:
			publish_date = datetime.utcnow()
		post = BlogPost(title=title, body=body, publish_date=publish_date, posted=posted, slug=slug, featured_image=featured_image, video=video, author=current_user._get_current_object()	)
		db.session.add(post)
		db.session.commit()
		return redirect(url_for('main.blog'))
	return render_template('blog/new_post.html', form=form)


@blog.route('/<slug>', methods=['GET', 'POST'])
def post(slug):
	post = BlogPost.query.filter_by(slug=slug).first_or_404()
	comment = None
	form = CommentForm()
	if form.validate_on_submit():
		comment = BlogComments(author_name = form.name.data, author_email = form.email.data, body = form.body.data, notify = form.notify.data, approved=False, post=post)
	if comment:
		db.session.add(comment)
		db.session.commit()
		flash("Thanks you for your comment. It will be published shortly after it has been moderated")
		return redirect(url_for('.post', slug=post.slug) + '#top')
	comments = post.comments.order_by(BlogComments.publish_date.asc()).all()
	return render_template('/blog/post.html', post=post, form=form, comments=comments)


@blog.route('/edit/<slug>', methods=['POST', 'GET'])
def edit_post(slug):
	post = BlogPost.query.filter_by(slug=slug).first_or_404()
	form = PostForm(obj=post)
	if form.validate_on_submit():
		if form.featured_image.data:
			filename = secure_filename(form.featured_image.data.filename)
			extension = filename.rsplit('.', 1)[1]
			featured_image = str(uuid.uuid4()) + '.' + extension
			form.featured_image.data.save(os.path.join(current_app.config['UPLOAD_FOLDER'], featured_image))
		post.title = form.title.data
		post.body = form.body.data
		post.posted = form.posted.data
		post.video = form.video.data
		post.slug = re.sub('[^\w]+', '-', form.title.data.lower()).strip('-')
		db.session.add(post)
		db.session.commit()
		return redirect(url_for('main.blog'))
	return render_template('blog/new_post.html', form=form, post=post)
