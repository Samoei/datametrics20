from datetime import datetime
import hashlib
from flask import request
from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin
from . import db, login_manager
from markdown import markdown
import bleach
from sqlalchemy.schema import UniqueConstraint


class TrendTweetFetchLog(db.Model):
	__tablename__ = 'trend_tweets_fetch_log'
	id = db.Column(db.Integer, primary_key=True)
	fetched_start = db.Column(db.DateTime, nullable=False)
	fetched_end = db.Column(db.DateTime)
	last_tweet_id = db.Column(db.Integer)
	is_last = db.Column(db.Boolean)
	total_tweets_fetched = db.Column(db.Integer)
	tweets = db.relationship('TrendTweet', backref='fetch', lazy='dynamic')


class ScheduledTweets(db.Model):
	__tablename__ = 'scheduled_tweets'
	id = db.Column(db.Integer, primary_key=True)
	trend_id = db.Column(db.Integer, db.ForeignKey('twitter_trends.id'))
	tweet_text = db.Column(db.String(150), nullable=False)
	posted = db.Column(db.Boolean(), default=False)
	generated_on = db.Column(db.DateTime(), default=datetime.utcnow, nullable=False)
	postted_on = db.Column(db.DateTime())

class ScheduledTopTrending(db.Model):
	__tablename__ = 'scheduled_top_trending'
	id = db.Column(db.Integer, primary_key=True)
	tweet_text = db.Column(db.String(150), nullable=False)
	posted = db.Column(db.Boolean(), default=False)
	generated_on = db.Column(db.DateTime(), default=datetime.utcnow, nullable=False)
	postted_on = db.Column(db.DateTime())


class TrendTweet(db.Model):
	__tablename__ = 'trend_tweets'
	id = db.Column(db.Integer, primary_key=True)
	trend_id = db.Column(db.Integer, db.ForeignKey('twitter_trends.id'))
	fetch_id = db.Column(db.Integer, db.ForeignKey('trend_tweets_fetch_log.id'))
	text = db.Column(db.String(200), nullable=False)
	tweet_id = db.Column(db.String(500), nullable=False, unique=True, index=True)
	original_tweet_id = db.Column(db.String(500))
	retweet = db.Column(db.Boolean())
	tweeted_at = db.Column(db.DateTime, nullable=False)
	retweet_count = db.Column(db.Integer, default=0)
	favourite_count = db.Column(db.Integer, nullable=False, default=0)
	tweeted_by = db.Column(db.String(200), nullable=False)
	user_name = db.Column(db.String(200), nullable=False)
	profile_img_url = db.Column(db.String(120))
	tweeted_using = db.Column(db.String(150))
	source_url = db.Column(db.String(150), nullable=False)
	tweeted_from = db.Column(db.String(200))
	friends_count = db.Column(db.Integer, nullable=False, default=0)
	followers_count = db.Column(db.Integer, default=0)
	statuses_count = db.Column(db.Integer, default=0)
	verified = db.Column(db.Boolean)
	polarity = db.Column(db.Integer)
	entities = db.relationship('TrendTweetEntities', backref='entity', lazy='dynamic')


class KeywordTweet(db.Model):
	__tablename__ = 'keyword_tweets'
	id = db.Column(db.Integer, primary_key=True)
	keyword_id = db.Column(db.Integer, db.ForeignKey('twitter_keywords.id'))
	text = db.Column(db.String(200), nullable=False)
	tweet_id = db.Column(db.String(500), nullable=False, unique=True, index=True)
	original_tweet_id = db.Column(db.String(500))
	retweet = db.Column(db.Boolean())
	tweeted_at = db.Column(db.DateTime, nullable=False)
	retweet_count = db.Column(db.Integer, default=0)
	favourite_count = db.Column(db.Integer, nullable=False, default=0)
	tweeted_by = db.Column(db.String(200), nullable=False)
	user_name = db.Column(db.String(200), nullable=False)
	profile_img_url = db.Column(db.String(120))
	tweeted_using = db.Column(db.String(150))
	source_url = db.Column(db.String(150), nullable=False)
	tweeted_from = db.Column(db.String(200))
	friends_count = db.Column(db.Integer, nullable=False, default=0)
	followers_count = db.Column(db.Integer, default=0)
	statuses_count = db.Column(db.Integer, default=0)
	verified = db.Column(db.Boolean)
	polarity = db.Column(db.Integer)
	# entities = db.relationship('KeywordTweetEntities', backref='entity', lazy='dynamic')


class TwitterKeyword(db.Model):
	__tablename__ = 'twitter_keywords'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(128), nullable=False)
	trending = db.Column(db.Boolean(), default=True)
	started = db.Column(db.DateTime(), default=datetime.utcnow, nullable=False)
	ended = db.Column(db.DateTime())
	updated_date = db.Column(db.DateTime(), nullable=False)
	position = db.Column(db.Integer)
	prev_position = db.Column(db.Integer)
	summarized = db.Column(db.Boolean())
	is_active = db.Column(db.Boolean())
	total_tweets = db.Column(db.Integer)
	total_retweets = db.Column(db.Integer)
	total_users = db.Column(db.Integer)
	tweets = db.relationship('KeywordTweet', backref='keyword', lazy='dynamic')
	top_tweets = db.relationship('KeywordTopTweets', backref='keyword', lazy='dynamic')
	apps = db.relationship('KeywordAppUsage', backref='keyword', lazy='dynamic')
	users_activity = db.relationship('KeywordUserActivity', backref='keyword', lazy='dynamic')
	search_terms = db.relationship('KeywordSearchTerm', backref='searchterms', lazy='dynamic')


class KeywordSearchTerm(db.Model):
	__tablename__ = 'keyword_search_terms'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(128), nullable=False)
	keyword_id = db.Column(db.Integer, db.ForeignKey('twitter_keywords.id'))


class KeywordUserActivity(db.Model):
	__tablename__ = 'keyword_users_activity'
	id = db.Column(db.Integer, primary_key=True)
	screen_name = db.Column(db.String(150), nullable=False,)
	keyword_id = db.Column(db.Integer, db.ForeignKey('twitter_keywords.id'))
	tweets = db.Column(db.Integer)
	retweets = db.Column(db.Integer)
	profile_img_url = db.Column(db.String(120))
	followers_count = db.Column(db.Integer, default=0)
	updated = db.Column(db.DateTime(), default=datetime.utcnow)
	__table_args__ = (UniqueConstraint('keyword_id', 'screen_name', name='_keyword_user_uc'),)


class KeywordAppUsage(db.Model):
	__tablename__ = 'keyword_app_usage'
	id = db.Column(db.Integer, primary_key=True)
	app_name = db.Column(db.String(150), nullable=False,)
	count = db.Column(db.Integer)
	keyword_id = db.Column(db.Integer, db.ForeignKey('twitter_keywords.id'))
	updated = db.Column(db.DateTime(), default=datetime.utcnow)


class KeywordTopTweets(db.Model):
	__tablename__ = 'keyword_top_tweets'
	id = db.Column(db.Integer, primary_key=True)
	keyword_id = db.Column(db.Integer, db.ForeignKey('twitter_keywords.id'))
	updated = db.Column(db.DateTime(), default=datetime.utcnow)
	text = db.Column(db.String(200), nullable=False)
	tweet_id = db.Column(db.String(500), nullable=False)
	tweeted_at = db.Column(db.DateTime, nullable=False)
	retweet_count = db.Column(db.Integer, default=0)
	favourite_count = db.Column(db.Integer, nullable=False, default=0)
	handle = db.Column(db.String(200), nullable=False)
	name = db.Column(db.String(200), nullable=False)
	profile_img_url = db.Column(db.String(120))
	tweeted_using = db.Column(db.String(150))
	source_url = db.Column(db.String(150), nullable=False)
	tweeted_from = db.Column(db.String(200))
	friends_count = db.Column(db.Integer, nullable=False, default=0)
	followers_count = db.Column(db.Integer, default=0)
	statuses_count = db.Column(db.Integer, default=0)
	verified = db.Column(db.Boolean)
	__table_args__ = (UniqueConstraint('keyword_id', 'tweet_id', name='_keyword_tweet_uc'),)


def dump_datetime(value):
	"""Deserialize datetime object into string form for JSON processing."""
	if value is None:
		return None
	return value.strftime('%Y-%m-%d %H:%M:%S')


class StreamedTweets(db.Model):
	__tablename__ = 'streamed_tweets'
	id = db.Column(db.Integer, primary_key=True)
	updated = db.Column(db.DateTime(), default=datetime.utcnow)
	text = db.Column(db.String(200), nullable=False)
	tweet_id = db.Column(db.String(500), nullable=False)
	tweeted_at = db.Column(db.DateTime, nullable=False)
	retweet_count = db.Column(db.Integer, default=0)
	favourite_count = db.Column(db.Integer, nullable=False, default=0)
	handle = db.Column(db.String(200), nullable=False)
	name = db.Column(db.String(200), nullable=False)
	profile_img_url = db.Column(db.String(120))
	tweeted_using = db.Column(db.String(150))
	tweeted_from = db.Column(db.String(200))
	friends_count = db.Column(db.Integer, nullable=False, default=0)
	followers_count = db.Column(db.Integer, default=0)
	statuses_count = db.Column(db.Integer, default=0)
	polarity_value = db.Column(db.Float(),)
	polarity_desc = db.Column(db.String(30),)
	subjectivity_value = db.Column(db.Float(),)
	subjectivity_desc = db.Column(db.String(30),)
	processed = db.Column(db.Boolean, default=False)
	searchterm = db.Column(db.String(40),)

	@property
	def serialize(self):
		"""Return object data in easily serializeable format"""
		return {
			'id': self.id,
			'searchterm': self.searchterm,
			'subjectivity_desc': self.subjectivity_desc,
			'polarity_desc': self.polarity_desc,
			'tweeted_at': dump_datetime(self.tweeted_at),
			'handle': self.handle,
			'tweeted_using': self.tweeted_using,
			'text': self.text
		}


class TrendTweetEntities(db.Model):
	__tablename__ = 'trend_tweets_entities'
	id = db.Column(db.Integer, primary_key=True)
	tweet_id = db.Column(db.Integer, db.ForeignKey('trend_tweets.id'))
	entity_id = db.Column(db.Integer, db.ForeignKey('twitter_entity_type.id'))
	name = db.Column(db.String(150), nullable=False)


class TwitterEntityType(db.Model):
	__tablename__ = 'twitter_entity_type'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50), nullable=False, index=True, unique=True)
	entity_types = db.relationship('TrendTweetEntities', backref='entity_type', lazy='dynamic')


class TrendUserActivity(db.Model):
	__tablename__ = 'trend_users_activity'
	id = db.Column(db.Integer, primary_key=True)
	screen_name = db.Column(db.String(150), nullable=False,)
	trend_id = db.Column(db.Integer, db.ForeignKey('twitter_trends.id'))
	tweets = db.Column(db.Integer)
	retweets = db.Column(db.Integer)
	profile_img_url = db.Column(db.String(120))
	followers_count = db.Column(db.Integer, default=0)
	updated = db.Column(db.DateTime(), default=datetime.utcnow)
	__table_args__ = (UniqueConstraint('trend_id', 'screen_name', name='_trend_user_uc'),
                     )


class TrendAppUsage(db.Model):
	__tablename__ = 'trend_app_usage'
	id = db.Column(db.Integer, primary_key=True)
	app_name = db.Column(db.String(150), nullable=False,)
	count = db.Column(db.Integer)
	trend_id = db.Column(db.Integer, db.ForeignKey('twitter_trends.id'))
	updated = db.Column(db.DateTime(), default=datetime.utcnow)


class TrendTopTweets(db.Model):
	__tablename__ = 'trend_top_tweets'
	id = db.Column(db.Integer, primary_key=True)
	trend_id = db.Column(db.Integer, db.ForeignKey('twitter_trends.id'))
	updated = db.Column(db.DateTime(), default=datetime.utcnow)
	text = db.Column(db.String(200), nullable=False)
	tweet_id = db.Column(db.String(500), nullable=False)
	tweeted_at = db.Column(db.DateTime, nullable=False)
	retweet_count = db.Column(db.Integer, default=0)
	favourite_count = db.Column(db.Integer, nullable=False, default=0)
	handle = db.Column(db.String(200), nullable=False)
	name = db.Column(db.String(200), nullable=False)
	profile_img_url = db.Column(db.String(120))
	tweeted_using = db.Column(db.String(150))
	source_url = db.Column(db.String(150), nullable=False)
	tweeted_from = db.Column(db.String(200))
	friends_count = db.Column(db.Integer, nullable=False, default=0)
	followers_count = db.Column(db.Integer, default=0)
	statuses_count = db.Column(db.Integer, default=0)
	verified = db.Column(db.Boolean)
	__table_args__ = (UniqueConstraint('trend_id', 'tweet_id', name='_trend_tweet_uc'),
                     )


class TwitterUser(db.Model):
	__tablename__ = 'twitter_users'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(200), nullable=False)
	twitter_user_id = db.Column(db.String(200), nullable=False, unique=True, index=True)
	screen_name = db.Column(db.String(150), nullable=False, unique=True, index=True)
	followers_count = db.Column(db.Integer)
	friends_count = db.Column(db.Integer)
	status_count = db.Column(db.Integer)
	listed_count = db.Column(db.Integer)
	favourites_count = db.Column(db.Integer)
	description = db.Column(db.String(500))
	location = db.Column(db.String(64))
	verified = db.Column(db.Boolean())
	is_coperate = db.Column(db.Boolean())
	is_celeb = db.Column(db.Boolean())
	is_politician = db.Column(db.Boolean())
	is_personal = db.Column(db.Boolean())
	is_premium = db.Column(db.Boolean())
	created_at = db.Column(db.DateTime())
	tracking_start = db.Column(db.DateTime())
	last_updated = db.Column(db.DateTime())
	lang = db.Column(db.String(30))
	profile_img_url = db.Column(db.String(120))
	category_id = db.Column(db.Integer, db.ForeignKey('users_category.id'))
	meta = db.relationship('TwitterUserMeta', backref='meta')


class UserCategory(db.Model):
	__tablename__ = 'users_category'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(30))
	user = db.relationship('TwitterUser', backref='user')


class TwitterUserMeta(db.Model):
	__tablename__ = 'twitter_users_meta'
	id = db.Column(db.Integer, primary_key=True)
	followers_count = db.Column(db.Integer)
	friends_count = db.Column(db.Integer)
	status_count = db.Column(db.Integer)
	listed_count = db.Column(db.Integer)
	favourites_count = db.Column(db.Integer)
	as_at = db.Column(db.DateTime(), default=datetime.utcnow)
	twitter_user_id = db.Column(db.Integer, db.ForeignKey('twitter_users.id'))


class TwitterTrend(db.Model):
	__tablename__ = 'twitter_trends'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(128), nullable=False)
	trending = db.Column(db.Boolean(), default=True)
	started = db.Column(db.DateTime(), default=datetime.utcnow, nullable=False)
	ended = db.Column(db.DateTime())
	updated_date = db.Column(db.DateTime(), nullable=False)
	position = db.Column(db.Integer)
	prev_position = db.Column(db.Integer)
	query = db.Column(db.String(128), nullable=False)
	url = db.Column(db.String(150), nullable=False)
	promoted = db.Column(db.Boolean())
	summarized = db.Column(db.Boolean())
	total_tweets = db.Column(db.Integer)
	total_retweets = db.Column(db.Integer)
	total_users = db.Column(db.Integer)
	tweets = db.relationship('TrendTweet', backref='trend', lazy='dynamic')
	top_tweets = db.relationship('TrendTopTweets', backref='trend', lazy='dynamic')
	apps = db.relationship('TrendAppUsage', backref='trend', lazy='dynamic')
	users_activity = db.relationship('TrendUserActivity', backref='trend', lazy='dynamic')
	scheduled_tweets = db.relationship('ScheduledTweets', backref='trend', lazy='dynamic')


allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'code', 'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p']

comments_allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'code', 'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']


class BlogPost(db.Model):
	__tablename__ = 'posts'
	id = db.Column(db.Integer, primary_key=True)
	slug = db.Column(db.String(128), nullable=False, unique=True, index=True)
	title = db.Column(db.String(150), nullable=False, index=True)
	body = db.Column(db.Text, nullable=False)
	body_html = db.Column(db.Text)
	posted = db.Column(db.Boolean(), default=False)
	featured_image = db.Column(db.String(128))
	video = db.Column(db.String(128))
	draft_date = db.Column(db.DateTime(), default=datetime.utcnow)
	publish_date = db.Column(db.DateTime())
	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	comments = db.relationship('BlogComments', lazy='dynamic', backref='post')

	@staticmethod
	def on_changed_body(target, value, oldvalue, initiator):
		target.body_html = bleach.linkify(bleach.clean(markdown(value, output_format='html'), tags=comments_allowed_tags, strip=True))


db.event.listen(BlogPost.body, 'set', BlogPost.on_changed_body)


class BlogComments(db.Model):
	__tablename__ = 'blog_comments'
	id = db.Column(db.Integer, primary_key=True)
	body = db.Column(db.Text, nullable=False)
	body_html = db.Column(db.Text)
	author_name = db.Column(db.String(128))
	author_email = db.Column(db.String(64))
	publish_date = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
	approved = db.Column(db.Boolean, default=False)
	notify = db.Column(db.Boolean, default=True)
	post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))

	@staticmethod
	def on_changed_body(target, value, oldvalue, initiator):
		target.body_html = bleach.linkify(bleach.clean(markdown(value, output_format='html'), tags=allowed_tags, strip=True))


db.event.listen(BlogComments.body, 'set', BlogComments.on_changed_body)


class User(UserMixin, db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(64), nullable=False, unique=True, index=True)
	username = db.Column(db.String(64), nullable=False, unique=True, index=True)
	is_admin = db.Column(db.Boolean)
	password_harsh = db.Column(db.String(128))
	first_name = db.Column(db.String(64))
	last_name = db.Column(db.String(64))
	location = db.Column(db.String(64))
	bio = db.Column(db.Text)
	member_since = db.Column(db.DateTime(), default=datetime.utcnow)
	avatar_hash = db.Column(db.String(32))
	posts = db.relationship('BlogPost', backref='author', lazy='dynamic')

	def __init__(self, **kwargs):
		super(User, self).__init__(**kwargs)
		if self.email is not None and self.avatar_hash is None:
			self.avatar_hash = hashlib.md5(self.email.encode('utf-8')).hexdigest()

	@property
	def password(self):
		raise AttributeError('password is not a readable attribute')

	@password.setter
	def password(self, password):
		self.password_harsh = generate_password_hash(password)

	def verify_password(self, password):
		return check_password_hash(self.password_harsh, password)

	@login_manager.user_loader
	def load_user(user_id):
		return User.query.get(int(user_id))

	def gravatar(self, size=100, default='identicon', rating='g'):
		if request.is_secure:
			url = 'https://secure.gravatar.com/avatar'
		else:
			url = 'https://www.gravatar.com/avatar'
		hash = self.avatar_hash or hashlib.md5(self.email.encode('utf-8')).hexdigest()
		return '{url}/{hash}?s={size}&d={default}&r={rating}'.format(url=url, hash=hash, size=size, default=default, rating=rating)
